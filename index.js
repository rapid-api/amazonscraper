const express = require('express');
const request = require('request-promise');

const PORT = process.env.PORT || 5000;
const app = express();

const rapidapi_header = process.env.RAPID_API_PROXY_SECRET || '';

app.use(express.json());

app.use((req, res, next) => {
    if ( req.headers['x-rapidapi-proxy-secret']   !== rapidapi_header) {
      return res.status(500).json({error: "Not authorized", status: 500});
    }
});

// Welcome route
app.get('/', async (req, res) => {
    res.send('Welcome to Amazon Scraper API!');
});

// Get product details
app.get('/products/:productName', async (req, res) => {
    const { productName } = req.params;
    const { api_key } = req.query;
    res.status(200).json({status: "success", passed_api:api_key,name:productName});    
});


app.listen(PORT, () => console.log(`Server Running on Port: ${PORT}`));